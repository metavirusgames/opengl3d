package com.healingpotion.graphics;

import com.healingpotion.utils.MathUtil;

public class Color {

	public int a;
	
	public int r;
	
	public int g;
	
	public int b;
	
	public final static Color black = new Color(0, 0, 0, 255);
	
	public final static Color white = new Color(255, 255, 255, 255);
	
	public Color(int r, int g, int b){
		this.r = MathUtil.clamp(r, 0, 255);
		this.g = MathUtil.clamp(g, 0, 255);
		this.b = MathUtil.clamp(b, 0, 255);
		this.a = MathUtil.clamp(255, 0, 255);
	}
	
	public Color(int r, int g, int b, int a){
		this(r, g, b);
		this.a = MathUtil.clamp(a, 0, 255);
	}
	
	public Color(Color color, int a){
		this(color.r, color.g, color.b, a);
	}
}
