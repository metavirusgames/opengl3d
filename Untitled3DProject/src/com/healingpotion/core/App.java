package com.healingpotion.core;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

import com.healingpotion.utils.CoreUtil;

public class App {

	private static final String window = "Healing Potion 3D";
	private boolean running = true;
	private IGame game;
	
	public void run(){
		
		game = new Game();
		
		createWindow();
		
		game.init();
		
		while (running){		
			tick();

			render();
			
			pollInput();
			Display.update();
		}
		
		destroy();
	}
	
	private void tick(){
		//Display.sync(60);
		
		game.update();
	}
	
	private void render(){
		game.render();
	}
	
	private void pollInput(){
		
		while (Keyboard.next()){
			if (Keyboard.getEventKeyState()){
				if (Keyboard.getEventKey() == Keyboard.KEY_ESCAPE) running = false;
			}
		}
		
		if (Display.isCloseRequested()){
			running = false;
		}
	}
	
	private void createWindow(){
		try {
			Display.setDisplayMode(new DisplayMode(1024, 768));
			Display.setVSyncEnabled(false);
			Display.setResizable(true);
			Display.setTitle(window);
			try {
				Display.setIcon(CoreUtil.loadIcon(new File("res/icon.png").toURL()));
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Display.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
			System.exit(0);
		}
		
	}
	
	private void destroy(){
		Display.destroy();
	}
	
	public static void main(String[] args){
		
		App app = new App();
		app.run();
	}
}
