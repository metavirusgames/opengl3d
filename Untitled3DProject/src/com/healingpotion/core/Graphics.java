package com.healingpotion.core;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import net.java.games.input.Keyboard;

import org.lwjgl.Sys;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;
import org.lwjgl.util.glu.Sphere;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.omg.CORBA.PRIVATE_MEMBER;

import com.healingpotion.graphics.Color;

public class Graphics {
	
	float triangleAngle; // Angle of rotation for the triangle
	float quadAngle; // Angle of rotation for the quad
	float lastFrameTime; // used to calculate delta
	float zoom = -15.0f;
	Vector3f position = new Vector3f(0, 0, zoom);
	Vector2f lastMousePosition;
	float yaw = 0f;
	float pitch = 0f;
	float mouseSensitivity = 0.05f;
	private Texture texture;
	private Texture dirtTexture;
	
	public int getWidth(){
		return Display.getWidth();
	}
	
	public int getHeight(){
		return Display.getHeight();
	}
	
	public void clear(Color color){
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
		GL11.glClearColor(color.r, color.g, color.b, color.a);
	}
	
	public void init(){
		Mouse.setGrabbed(true);
		GL11.glViewport(0, 0, getWidth(), getHeight());
		GL11.glMatrixMode(GL11.GL_PROJECTION); // Select The Projection Matrix
		GL11.glLoadIdentity(); // Reset The Projection Matrix
		GLU.gluPerspective(45.0f, ((float) getWidth() / (float) getHeight()), 0.1f, 100.0f); // Calculate The Aspect Ratio Of The Window
		GL11.glMatrixMode(GL11.GL_MODELVIEW); // Select The Modelview Matrix
		GL11.glLoadIdentity(); // Reset The Modelview Matrix
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glShadeModel(GL11.GL_SMOOTH); // Enables Smooth Shading
		//GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f	 ); // Black Background
		clear(new Color(Color.black, 0));
		
		GL11.glClearDepth(3.0f); // Depth Buffer Setup
		GL11.glEnable(GL11.GL_DEPTH_TEST); // Enables Depth Testing
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glDepthFunc(GL11.GL_LEQUAL); // The Type Of Depth Test To Do
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);	
		GL11.glLightModeli(GL11.GL_LIGHT_MODEL_TWO_SIDE,GL11.GL_TRUE);
		//GL11.glEnable(GL11.GL_COLOR_MATERIAL);
		GL11.glHint(GL11.GL_PERSPECTIVE_CORRECTION_HINT, GL11.GL_NICEST); // Really Nice Perspective Calculations

		
		float lightAmbient[] = {1.0f, 1.0f, 1.0f, 0.2f};  // Ambient Light Values
        float lightDiffuse[] = {1.0f, 1.0f, 1.0f, 0.0f};      // Diffuse Light Values
        float lightPosition[] = {2.9f, -2.1f, -7.1f, 1.0f}; // Light Position
        float lightSpecular[] = {0.0f, 0.0f, 0.0f, 0.0f};
        ByteBuffer temp = ByteBuffer.allocateDirect(16);
        temp.order(ByteOrder.nativeOrder());
        GL11.glLight(GL11.GL_LIGHT1, GL11.GL_AMBIENT, (FloatBuffer) temp.asFloatBuffer().put(lightAmbient).flip());              // Setup The Ambient Light
        GL11.glLight(GL11.GL_LIGHT1, GL11.GL_DIFFUSE, (FloatBuffer) temp.asFloatBuffer().put(lightDiffuse).flip());              // Setup The Diffuse Light
        GL11.glLight(GL11.GL_LIGHT1, GL11.GL_POSITION, (FloatBuffer) temp.asFloatBuffer().put(lightPosition).flip());         // Position The Light
        GL11.glLight(GL11.GL_LIGHT1, GL11.GL_SPECULAR, (FloatBuffer) temp.asFloatBuffer().put(lightSpecular).flip());
        GL11.glEnable(GL11.GL_LIGHT1); 
        
        
        try {
			texture = TextureLoader.getTexture("PNG", new FileInputStream("res/texture.png"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        try {
			dirtTexture = TextureLoader.getTexture("PNG", new FileInputStream("res/dirt.png"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void update(float delta){
		
		if (Display.wasResized()){
			init();
		}
		
		float distance = 0.15f;
		
		if (org.lwjgl.input.Keyboard.isKeyDown(org.lwjgl.input.Keyboard.KEY_W)){
			position.x -= distance  * (float)Math.sin(Math.toRadians(yaw));
			position.z += distance * (float)Math.cos(Math.toRadians(yaw));
		}
		
		if (org.lwjgl.input.Keyboard.isKeyDown(org.lwjgl.input.Keyboard.KEY_S)){
			position.x += distance * (float)Math.sin(Math.toRadians(yaw));
		    position.z -= distance * (float)Math.cos(Math.toRadians(yaw));
		}
		
		if (org.lwjgl.input.Keyboard.isKeyDown(org.lwjgl.input.Keyboard.KEY_A)){
			position.x -= distance * (float)Math.sin(Math.toRadians(yaw-90));
		    position.z += distance * (float)Math.cos(Math.toRadians(yaw-90));
		}
		
		if (org.lwjgl.input.Keyboard.isKeyDown(org.lwjgl.input.Keyboard.KEY_D)){
			position.x -= distance * (float)Math.sin(Math.toRadians(yaw+90));
		    position.z += distance * (float)Math.cos(Math.toRadians(yaw+90));
		}
		
		if (org.lwjgl.input.Keyboard.isKeyDown(org.lwjgl.input.Keyboard.KEY_LSHIFT)){
			position.y += distance;
		}
		
		if (org.lwjgl.input.Keyboard.isKeyDown(org.lwjgl.input.Keyboard.KEY_SPACE)){
			position.y -= distance;
		}
		
		//System.out.println(position);
		//triangleAngle += 0.2f * delta; // Increase The Rotation Variable For The Triangle
		//quadAngle -= 0.2f * delta; // Decrease The Rotation Variable For The Quad
		yaw += Mouse.getDX() * mouseSensitivity;
		pitch -= Mouse.getDY() * mouseSensitivity;
		
		Display.setTitle(position.toString());
		
		Display.sync(60);
	}
	
	public void render(){

		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT); // Clear The Screen And The Depth Buffer
		
		GL11.glLoadIdentity(); // Reset The View
		GL11.glRotatef(pitch, 1.0f, 0.0f, 0.0f); // Rotate The Cube On X, Y & Z
		GL11.glRotatef(yaw, 0.0f, 1.0f, 0.0f); // Rotate The Cube On X, Y & Z
		GL11.glTranslatef(position.x, position.y, position.z);
		
		int maxZ = 6;
		int maxX = 6;
		int maxY = 2;
		
		
		
		for (int y = 0; y < maxY; y++){
			GL11.glPushMatrix();
			GL11.glTranslatef(0, -y, 0);
			for (int z = 0; z < maxZ; z++){
				GL11.glPushMatrix();
				GL11.glTranslatef(0, 0, -z); // Move Right And Into The Screen
				for (int x = 0; x < maxX; x++){
					GL11.glPushMatrix();
					GL11.glTranslatef(-x, 0, 0); // Move Right And Into The Screen
	
					if (y == 0){
						if (x == 1 && z == 2){
							renderCube();
						}
					}
					else {
						renderCube();
					}
					
					
										
					GL11.glPopMatrix();
				}
				GL11.glPopMatrix();
			}
			GL11.glPopMatrix();
		}
		
		
		
		
	}
	
	public void renderCube(){
		
		texture.bind();
		
		GL11.glBegin(GL11.GL_QUADS); // Start Drawing The Cube
		
		/** TOP **/
		GL11.glNormal3f( 0.0f, 1.0f, 0.0f);    

		GL11.glTexCoord2f(1.0f, 1.0f);
		GL11.glVertex3f(0.5f, 0.5f, -0.5f); // Top Right Of The Quad (Top)
		
		GL11.glTexCoord2f(0.0f, 1.0f);
		GL11.glVertex3f(-0.5f, 0.5f, -0.5f); // Top Left Of The Quad (Top)
		
		GL11.glTexCoord2f(0.0f, 0.0f);
		GL11.glVertex3f(-0.5f, 0.5f, 0.5f); // Bottom Left Of The Quad (Top)
		
		GL11.glTexCoord2f(1.0f, 0.0f);
		GL11.glVertex3f(0.5f, 0.5f, 0.5f); // Bottom Right Of The Quad (Top)
		
		GL11.glEnd(); // Done Drawing The Quad
		
		dirtTexture.bind();
		
		GL11.glBegin(GL11.GL_QUADS); // Start Drawing The Cube
		
		/** BOTTOM **/
		GL11.glNormal3f( 0.0f,-1.0f, 0.0f);

		GL11.glVertex3f(0.5f, -0.5f, 0.5f); // Top Right Of The Quad (Bottom)
		
		GL11.glVertex3f(-0.5f, -0.5f, 0.5f); // Top Left Of The Quad (Bottom)
		
		GL11.glVertex3f(-0.5f, -0.5f, -0.5f); // Bottom Left Of The Quad (Bottom)
		
		GL11.glVertex3f(0.5f, -0.5f, -0.5f); // Bottom Right Of The Quad (Bottom)

		/** FRONT **/
		GL11.glNormal3f( 0.0f, 0.0f, 1.0f);
		
		GL11.glTexCoord2f(1.0f, 1.0f);
		GL11.glVertex3f(0.5f, 0.5f, 0.5f); // Top Right Of The Quad (Front)
		
		GL11.glTexCoord2f(0.0f, 1.0f);
		GL11.glVertex3f(-0.5f, 0.5f, 0.5f); // Top Left Of The Quad (Front)
		
		GL11.glTexCoord2f(0.0f, 0.0f);
		GL11.glVertex3f(-0.5f, -0.5f, 0.5f); // Bottom Left Of The Quad (Front)
		
		GL11.glTexCoord2f(1.0f, 0.0f);
		GL11.glVertex3f(0.5f, -0.5f, 0.5f); // Bottom Right Of The Quad (Front)
		
		/** BACK **/
		GL11.glNormal3f( 0.0f, 0.0f,-1.0f); 

		GL11.glTexCoord2f(1.0f, 1.0f);
		GL11.glVertex3f(-0.5f, 0.5f, -0.5f); // Top Right Of The Quad (Back)
		
		
		GL11.glTexCoord2f(0.0f, 1.0f);
		GL11.glVertex3f(0.5f, 0.5f, -0.5f); // Top Left Of The Quad (Back)	
		
		GL11.glTexCoord2f(0.0f, 0.0f);
		GL11.glVertex3f(0.5f, -0.5f, -0.5f); // Bottom Left Of The Quad (Back)
		
		GL11.glTexCoord2f(1.0f, 0.0f);
		GL11.glVertex3f(-0.5f, -0.5f, -0.5f); // Bottom Right Of The Quad (Back)

		/** LEFT **/
		GL11.glNormal3f(-1.0f, 0.0f, 0.0f);

		GL11.glTexCoord2f(1.0f, 1.0f);
		GL11.glVertex3f(-0.5f, 0.5f, 0.5f); // Top Right Of The Quad (Left)
		
		GL11.glTexCoord2f(0.0f, 1.0f);
		GL11.glVertex3f(-0.5f, 0.5f, -0.5f); // Top Left Of The Quad (Left)
		
		GL11.glTexCoord2f(0.0f, 0.0f);
		GL11.glVertex3f(-0.5f, -0.5f, -0.5f); // Bottom Left Of The Quad (Left)
		
		GL11.glTexCoord2f(1.0f, 0.0f);
		GL11.glVertex3f(-0.5f, -0.5f, 0.5f); // Bottom Right Of The Quad (Left)

		/** RIGHT **/
		GL11.glNormal3f( 1.0f, 0.0f, 0.0f);

		GL11.glTexCoord2f(1.0f, 1.0f);
		GL11.glVertex3f(0.5f, 0.5f, -0.5f); // Top Right Of The Quad (Right)
		
		GL11.glTexCoord2f(0.0f, 1.0f);
		GL11.glVertex3f(0.5f, 0.5f, 0.5f); // Top Left Of The Quad (Right)
		
		GL11.glTexCoord2f(0.0f, 0.0f);
		GL11.glVertex3f(0.5f, -0.5f, 0.5f); // Bottom Left Of The Quad (Right)
		
		GL11.glTexCoord2f(1.0f, 0.0f);
		GL11.glVertex3f(0.5f, -0.5f, -0.5f); // Bottom Right Of The Quad (Right)
		
		
		
		GL11.glEnd(); // Done Drawing The Quad
	}
	
	public float getDelta() {
	    long time = (Sys.getTime() * 1000) / Sys.getTimerResolution();
	    float delta = (float)(time - lastFrameTime);
	    lastFrameTime = time;
	 
	    return delta;
	}
}
