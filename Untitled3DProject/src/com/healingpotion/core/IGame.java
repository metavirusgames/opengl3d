package com.healingpotion.core;

public interface IGame {

	void init();
	
	void update();
	
	void render();
}
