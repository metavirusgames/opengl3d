package com.healingpotion.core;

import com.healingpotion.graphics.Color;

public class Game implements IGame {

	public Graphics graphics;
	
	@Override
	public void init() {
		// TODO Auto-generated method stub
		graphics = new Graphics();
		graphics.init();
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		graphics.update(graphics.getDelta());
	}

	@Override
	public void render() {
		// TODO Auto-generated method stub
		graphics.render();
	}

}
