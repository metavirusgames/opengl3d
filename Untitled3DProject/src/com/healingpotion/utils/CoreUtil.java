package com.healingpotion.utils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.ByteBuffer;

import de.matthiasmann.twl.utils.PNGDecoder;

public class CoreUtil {

	public static ByteBuffer[] loadIcon(URL url) throws IOException {
        InputStream is = url.openStream();
        try {
            PNGDecoder decoder = new PNGDecoder(is);
            ByteBuffer bb = ByteBuffer.allocateDirect(decoder.getWidth()*decoder.getHeight()*4);
            decoder.decode(bb, decoder.getWidth()*4, PNGDecoder.Format.RGBA);
            bb.flip();
            return new ByteBuffer[]{ bb };
        } finally {
            is.close();
        }
    }
}
